import React from 'react';
import './home.scss';
import { Container, Row} from 'react-bootstrap';
import TitleComponent from '../../components/title/title.component';
import QuestionsComponent from '../../components/question/questions.component';
import OverviewComponent from '../../components/overview/overview.component';

const Home = () => {
    
    return (
        <div>
            <TitleComponent/>
            <div className="qustn-wrapper">
                <Container fluid className="px-0">
                    <Row className="mx-0">
                        <div className="col-md-8 px-0 pr-md-4">
                            <QuestionsComponent/>
                        </div>
                        <div className="col-md-4 px-0 pl-md-4">
                            <OverviewComponent/>
                        </div>
                    </Row>
                </Container>
            </div>
        </div>
    )
}

export default Home
