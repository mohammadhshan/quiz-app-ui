import React, {useState} from 'react';
import './questions.component.scss';
import { FaAngleDoubleRight, FaAngleDoubleLeft, FaArrowAltCircleRight } from "react-icons/fa";
import { Row, Col, Button } from 'react-bootstrap';

const QuestionsComponent = () => {
    const [qustnCount, setQustnCount]=useState(1);
    const qustn=[
        {
            title:<span>10 Women can complete work in 7 days and 10 childred take 14 days to complete the work. <br/>
            How many days will 5 women and 10 children take to complete the work?</span>,
            options:[
                {
                    key: "A",
                    value:"3 Days"
                },
                {
                    key: "B",
                    value:"5 Days"
                },
                {
                    key: "C",
                    value:"7 Days"
                },
                {
                    key: "D",
                    value:"Can't be determine"
                }
            ],
            status:'un-answered'
        },
        {
            title:<span>Loreum Ipsum dolor simet Loreum Ipsum dolor simet?</span>,
            options:[
                {
                    key: "A",
                    value:"loreum ipsum"
                },
                {
                    key: "B",
                    value:"loreum ipsum"
                },
                {
                    key: "C",
                    value:"loreum ipsum"
                },
                {
                    key: "D",
                    value:"loreum ipsum"
                }
            ],
            status:'un-answered'
        },
        {
            title:<span>10 Women can complete work in 7 days and 10 childred take 14 days to complete the work. <br/>
            How many days will 5 women and 10 children take to complete the work?</span>,
            options:[
                {
                    key: "A",
                    value:"3 Days"
                },
                {
                    key: "B",
                    value:"5 Days"
                },
                {
                    key: "C",
                    value:"7 Days"
                },
                {
                    key: "D",
                    value:"Can't be determine"
                }
            ],
            status:'un-answered'
        }
    ];
    const nextQustn = (event) =>{
        if (qustnCount < qustn.length){
            setQustnCount(qustnCount+1)
        }
    }
    const prevQustn = (event) =>{
        if (qustnCount > 1){
            setQustnCount(qustnCount-1)
        }
    }
    return (
        <div className="col px-lg-50 py-lg-40 py-md-15 qustn-container">
            <div className="qustn-title-container">
                <div className="qustn-num">
                    Question : <span>{qustnCount}</span>
                </div>
                <div className="q-np-container">
                    <div className="q-prev-btn" onClick={e=>{prevQustn(e)}}>
                        <i className="d-flex"><FaAngleDoubleLeft/></i>Previous
                    </div>
                    <span>|</span>
                    <div className="q-next-btn" onClick={e=>{nextQustn(e)}}>
                        Next <i className="d-flex"><FaAngleDoubleRight/></i>
                    </div>
                </div>
            </div>
            <div className="qustn-view-container">
                <div className="qustn-view">
                    {qustn[qustnCount-1].title}
                </div>
                <div className="qustn-obj-container">
                    <p>Options</p>
                    <div className="qustn-obj-view">
                        <Row className="choice-row mx-minus-10">
                            <Col md={6} className="choice-col-main">
                                <div className="choice-column">
                                    <label className="choice-column-inner">
                                        <input type="radio" value="A" name="objective-ans"></input>
                                        <div className="objective-ans-view">
                                            <span><i>{qustn[qustnCount-1].options[0].key}</i></span>
                                            <b>{qustn[qustnCount-1].options[0].value}</b>
                                        </div>
                                    </label>
                                </div>
                            </Col>
                            <Col md={6} className="choice-col-main">
                                <div className="choice-column">
                                    <label className="choice-column-inner">
                                        <input type="radio" value="B" name="objective-ans"></input>
                                        <div className="objective-ans-view">
                                            <span><i>{qustn[qustnCount-1].options[1].key}</i></span>
                                            <b>{qustn[qustnCount-1].options[1].value}</b>
                                        </div>
                                    </label>
                                </div>
                            </Col>
                        </Row>
                        <Row className="choice-row mx-minus-10">
                            <Col md={6} className="choice-col-main">
                                <div className="choice-column">
                                    <label className="choice-column-inner">
                                     <input type="radio" value="C" name="objective-ans"></input>
                                        <div className="objective-ans-view">
                                            <span><i>{qustn[qustnCount-1].options[2].key}</i></span>
                                            <b>{qustn[qustnCount-1].options[2].value}</b>
                                        </div>
                                    </label>
                                </div>
                            </Col>
                            <Col md={6} className="choice-col-main">
                                <div className="choice-column">
                                    <label className="choice-column-inner">
                                        <input type="radio" value="A" name="objective-ans"></input>
                                        <div className="objective-ans-view">
                                            <span><i>{qustn[qustnCount-1].options[3].key}</i></span>
                                            <b>{qustn[qustnCount-1].options[3].value}</b>
                                        </div>
                                    </label>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
            <div className="qustn-btn-wrapper">
                <div>
                    <Button variant="dark" size="md" disabled>Clear Response</Button>{" "}
                    <Button variant="dark" size="md" disabled>Bookmark</Button>
                </div>
                <div>
                    <Button variant="dark" size="md" className="qstn-next-btn">Save & Next <i><FaArrowAltCircleRight/></i></Button>
                </div>
            </div>
        </div>
    )
}

export default QuestionsComponent;