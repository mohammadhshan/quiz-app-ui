import React from 'react';
import "./title.component.scss";
import { Container} from 'react-bootstrap';
import { IoIosTimer } from "react-icons/io";

class TitleComponent extends React.Component {
    
    constructor() {
        super();
        this.state = { time: {}, seconds: 3000 };
        this.timer = 0;
    }

    secondsToTime = (secs) =>{
        let hours = Math.floor(secs / (60 * 60));

        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);

        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = Math.ceil(divisor_for_seconds);

        if(seconds<10){
            seconds="0"+seconds;
        }

        if(minutes<10){
            minutes="0"+minutes;
        }

        if(hours<10){
            hours="0"+hours;
        }

        let obj = {
        "h": hours,
        "m": minutes,
        "s": seconds,
        };
        return obj;
    }
    startTimer = () => {
        if (this.timer === 0 && this.state.seconds > 0) {
          this.timer = setInterval(this.countDown, 1000);
        }
      }
    
    countDown = () => {

        let seconds = this.state.seconds - 1;
        this.setState({
            time: this.secondsToTime(seconds),
            seconds: seconds,
        });
        
        if (seconds === 0) { 
            clearInterval(this.timer);
        }
    }
    componentDidMount(){
        let timeLeftVar = this.secondsToTime(this.state.seconds);
        this.setState({ time: timeLeftVar });
        this.startTimer()
    }
    render(){
        const {time}=this.state;
        return (
            <div className="home-title-wrapper">
                <Container fluid className="px-lg-50">
                    <div className="home-title-container">
                        <div className="exam-title">Title of Exam</div>
                        <div className="time-wrapper">
                            <div className="time-title"><i><IoIosTimer/></i> Time Remaining</div>
                            <div className="timer">
                                {time.h}:{time.m}:{time.s}
                            </div>
                        </div>
                    </div>{/* <!-- home-title-container -->*/}
                </Container>
            </div>
        )
    }
}

export default TitleComponent;
