import React from 'react';
import './overview.component.scss';
import { Button } from 'react-bootstrap';

const OverviewComponent = () => {
    const qustnCountBlocks= new Array(48);
    qustnCountBlocks.fill(1)
    const QuestionNumbers = qustnCountBlocks.map((q,index)=>{
        let classname = index === 0 ?"qstn-cq":'';
        return <span key={index} className={classname}>{index + 1}</span>;
    });

    return (
        <div className="col px-lg-50 py-lg-40 py-md-15 qstn-overview-container">
            <div className="qstn-overview-title">
                Question Overview
            </div>
            <div className="overview-num-wrapper">
                {QuestionNumbers}
                {/* Conditional span classNames are given below*
                    qstn-cq for Current Question
                    qstn-answered for Answered Question
                    qstn-un-answered for Unanswered Question
                    qstn-bookmarked for Bookmarked Question
                */}
            </div>
            <div className="overview-legend-wrapper">
                <div className="overview-legend answered">Answered</div>
                <div className="overview-legend un-answered">Unanswered</div>
                <div className="overview-legend bookmarked">Bookmarked</div>
                <div className="overview-legend c-question">Current Question</div>
            </div>
            <div className="exam-btn-container text-right">
                <Button size="lg" className="btn-exam-submit">Submit Exam</Button>
            </div>
        </div>
    )
}

export default OverviewComponent;